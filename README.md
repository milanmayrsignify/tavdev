# TAVHealth Docker Development Container
This is a Dockerfile for TAVHealth developers.

## How to Use
1. Build the Docker image  
   `docker build -t tavdev .`

2. Run the container  
   `docker run -itd --name tavdev tavdev /bin/bash`

3. Copy your user's ssh keys to the container  
   `docker cp ~/.ssh/ tavdev:/root/`

4. Exec a shell into the container  
   `docker exec -it -u 0 tavdev /bin/bash`