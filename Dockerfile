FROM ubuntu

# Install basic packages
RUN apt-get update && apt-get install -y \
  unzip \
  zip \
  curl \
  ssh \
  zsh \
  nano \
  vim
RUN apt-get update && apt-get install --assume-yes git
RUN curl -s "https://get.sdkman.io" | bash

# Install SDKs
RUN /bin/bash -c "source /root/.sdkman/bin/sdkman-init.sh; \
sdk version; \
sdk install java 11.0.13.8.1-amzn; \
sdk install java 8.0.302-zulu; \
sdk install grails 4.0.12; \
sdk install grails 3.3.11; \
sdk install groovy 2.5.14; \
sdk install groovy 2.4.17; \
sdk install gradle 7.3.1"

CMD ["/bin/bash"]